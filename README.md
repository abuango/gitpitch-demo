GitPitch Demo
=============

This is to demonstrate the usage of *GitPitch* for making presentations. You can simply create a **PITCHME.md** file and `git push`.

```
if (isset($_GET['wsdl'])) {
    $autodiscover = new Zend\Soap\AutoDiscover();
    $autodiscover->setClass('ZenithPayments')
        ->setUri('https://pgd.fptb.edu.ng/api/ProcessPayment.php');
    $autodiscover->handle();
} else {
    // pointing to the current file here
    $soap = new Zend\Soap\Server("https://pgd.fptb.edu.ng/api/ProcessPayment.php?wsdl");
    $soap->setClass('ZenithPayments');
    $soap->handle();

}
```

>This is intended as a quick reference and showcase. For more complete info, see John Gruber's original spec and the Github-flavored Markdown info page.