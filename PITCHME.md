GitPitch Demonstration
======================

This is a tool for making presentation from a markdown file. For more information you can access `gitpitch.com`.

Sample Code
===========
```
if (isset($_GET['wsdl'])) {
    $autodiscover = new Zend\Soap\AutoDiscover();
    $autodiscover->setClass('ZenithPayments')
        ->setUri('https://pgd.fptb.edu.ng/api/ProcessPayment.php');
    $autodiscover->handle();
} else {
    // pointing to the current file here
    $soap = new Zend\Soap\Server("https://pgd.fptb.edu.ng/api/ProcessPayment.php?wsdl");
    $soap->setClass('ZenithPayments');
    $soap->handle();

}
```